
var restAPI = angular.module("restAPI", ["ngResource"]);

restAPI.factory("RestAPI", function($resource, $http) {

	return {
		getVideos: function(apiURL) {
			return $resource(apiURL + "/videos/public/:page/:size", { page: "@page", size: "@size" }, { get: { method: "GET" } });
		},
		getRecommendations: function(apiURL, userDetails, records, callback) {
			var request = {
				method: "POST",
				url: apiURL + "/videos/public/recommendations",
				headers: {
					"Content-Type": undefined
				},
				data: {userId: userDetails.userId, language: userDetails.language, filteredVideos: "", records: records}
			};
			$http(request).success(function(data){
				callback(data);
			}).error(function(){
			
			});			
		},
		getUserDetails: function(apiURL, userId) {
			return $resource(apiURL + "/user/"+userId, {}, {get: {method: "GET"}});
		},
		getPublicVideos: function(apiURL){
            return $resource(apiURL + "/videos", {                
                page: "@page", 
                size: "@size", 
                catId: "@catId",
                lang: "@lang"
            }, {get: {method: "GET"}});
        },
        getCategories: function(apiURL){
            return $resource(apiURL + "/videos/categories", {                
                appname: "@appname",
                platform: "@platform",
                locale: "@locale",
                mcc: "@mcc",
                page: "@page", 
                size: "@size"
            }, {get: {method: "GET"}});
        }
	}
});
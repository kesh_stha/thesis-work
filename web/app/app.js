
var thesisworkApp = angular.module("thesisworkApp", ["ngRoute", "restAPI", "thesisworkConfig"]);

thesisworkApp.config(["$routeProvider", function($routeProvider) {
	$routeProvider
		.when("/", {templateUrl: "views/main.view.html", controller: "MainController"})
		.otherwise({ redirectTo: "/" });
}]);
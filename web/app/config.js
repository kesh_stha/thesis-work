thesisworkConfig = angular.module("thesisworkConfig", []);

thesisworkConfig.factory("Config", function() {    
    
    return {
        islandURLs: {            
            dev: { 
                thumbURL: "http://192.168.2.70:8084/thumbnail",
                apiURL: "http://192.168.2.70:9090/zoobe"
            },
            hq: { 
                thumbURL: "http://hq.zoobe.com/thumbnail",
                apiURL: "http://appphq.zoobe.com:9090/zoobe"                   
            },
            b: {                    
                thumbURL: "http://b.zoobe.com/thumbnail",
                apiURL: "https://apib.zoobe.com/zoobe"
            },
            d: { 
                thumbURL: "http://d.zoobe.com/thumbnail",
                apiURL: "https://apid.zoobe.com/zoobe"

            },
            s: {                     
                thumbURL: "http://s.zoobe.com/thumbnail",
                apiURL: "https://apis.zoobe.com/zoobe"
            },
            v: { 
                thumbURL: "http://v.zoobe.com/thumbnail",
                apiURL: "https://apiv.zoobe.com/zoobe"
            },
            default: {
                thumbURL: "",
                apiURL: "https://api.zoobe.com/zoobe"
            }
        },         
        scaleThumb: "0.25",
        app: {
            name: "Zoobe2",
            mcc: "-1"
        }        
    };
});

function MainController($rootScope, $scope, $sce, RestAPI, Config) {
	
	$rootScope.view = "main";
		
	var apiURL = "http://localhost:9090";
	var restURL = "https://apib.zoobe.com/zoobe/";
	var viewport_width = $(window).width();   // returns width of browser viewport
	var userLang = "en";
		
	$scope.selectUser = function(user) {
		// init		
		$rootScope.view = "videos";
		getVideos(0, user);
		//getUserDetails(user);
	};
	
	$rootScope.logout = function() {
		$rootScope.view = "main";
	};
	
	$rootScope.getVideos = function(offset) {
		$rootScope.view = "videos";
		getVideos(offset, $scope.user);		
	};
	
	$rootScope.getRecommendations = function() {
		$rootScope.view = "recommend";
		RestAPI.getRecommendations(apiURL, $rootScope.userDetails, 15, function(data) {
			$scope.recommendations = data;
			console.log(data);
		});
		console.log("recommendations");
	};
	
	$scope.thumbURL = function (thumbnail, video) {
        if (thumbnail != undefined) {
			if(/'\?'/.test(thumbnail)) {
				return thumbnail + "&dc=" + Config.scaleThumb * viewport_width;
			}
            return thumbnail + "?dc=" + Config.scaleThumb * viewport_width;
        }
        // create thumbnail url
        return Config.islandURLs[video.islandId]['thumbURL'] + "/" + video.id + ".jpg?dc=" + Config.scaleThumb * viewport_width;
    }
	
	$scope.secureUrl = function (url) {
        // angular considers corss domain url as unsafe url and
        // does not allow directly to embed it,
        // so make corss domain url as trusted url
        return $sce.trustAsResourceUrl(url);
    };
	
	var getUserDetails = function(userId) {
		RestAPI.getUserDetails(apiURL,userId).get().$promise.then(function(result){
			$rootScope.userDetails = result;
			console.log($rootScope.userDetails)
		});
	};
	var getVideos = function(offset, userId) {
		var pageNum = 0;
		if(offset != 0) {
			pageNum = $scope.pagination.current + offset;			
		}
		RestAPI.getUserDetails(apiURL,userId).get().$promise.then(function(user){
			$rootScope.userDetails = user;
			console.log($rootScope.userDetails)
			RestAPI.getCategories(restURL)
				.get({appname: Config.app.name, platform: "iOS", locale: user.language, mcc: Config.app.mcc, page: 0, size: 10})
				.$promise.then(function(result) {			
					// set category to default value   
					var categoryId = result.content[0].id || "544659ef210e62f142831b46";
					// get list of videos for particular category and language
					RestAPI.getPublicVideos(restURL)
							.get({page: pageNum, size: 15, catId: categoryId, lang: user.language})
							.$promise.then(function(data) {
								$scope.pagination = {
									current: data.number,
									pages: data.totalPages,
									records: data.numberOfElements,
									totalRecords: data.totalElements,
									next: !data.lastPage,
									prev: !data.firstPage
								};
								$scope.videos = data.content;
								console.log(data);
					});
			});
		});
		
		/*
		RestAPI.getVideos(apiURL).get({page: pageNum, size: 20}).$promise.then(function(data) {
			$scope.pagination = {
				current: data.number,
				pages: data.totalPages,
				records: data.numberOfElements,
				totalRecords: data.totalElements,
				next: !data.last,
				prev: !data.first
			};
			$scope.videos = data.content;
			console.log($scope.videos);
		});
		*/
	};
	
}
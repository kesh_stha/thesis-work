package com.thesiswork.repositories.mongo;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import com.thesiswork.model.StoryUsage;

/**
*
* @author keshant
*/
@Component
public interface StoryUsageRepository extends MongoRepository<StoryUsage, Serializable> {

	public List<StoryUsage> findByValueLanguage(String language);
	
	public List<StoryUsage> findByValueLanguage(String language, Sort sort);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.repositories.mongo;

import com.thesiswork.model.PublicVideo;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.stereotype.Component;

/**
 *
 * @author keshant
 */
@Component
public interface PublicVideoRepository extends MongoRepository<PublicVideo, String> {

	public List<PublicVideo> findByLanguage(String language);
    
    public List<PublicVideo> findByLanguageAndIdNotIn(String language, String[] videoIds);
    
    public Page<PublicVideo> findByLanguage(String language, Pageable pgbl);
    
    public Page<PublicVideo> findByLanguageAndIdNotIn(String language, String[] videoIds, Pageable pgbl);
    
    public Page<PublicVideo> findByIdNotIn(String[] videoIds, Pageable pgbl);
            
	public List<PublicVideo> findByMetaBundleString(String bundleString);
    
    public List<PublicVideo> findByTitleRegex(String title);
    
}

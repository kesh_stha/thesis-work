package com.thesiswork.repositories.mongo;

import java.io.Serializable;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import com.thesiswork.model.Profile;
/**
*
* @author keshant
*/
@Component
public interface ProfileRepository extends MongoRepository<Profile, Serializable> {
		
	public Profile findByUserId(String userId);

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.repositories.mongo;

import com.thesiswork.model.UserStory;
import java.io.Serializable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

/**
 *
 * @author keshant
 */
@Component
public interface UserStoryRepository extends MongoRepository<UserStory, Serializable> {    
        
}

package com.thesiswork.repositories.mongo;

import java.io.Serializable;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import com.thesiswork.model.GeneratedCluster;

@Component
public interface GeneratedClusterRepository extends MongoRepository<GeneratedCluster, Serializable> {
	
	public GeneratedCluster findByMembers(String userId);

}

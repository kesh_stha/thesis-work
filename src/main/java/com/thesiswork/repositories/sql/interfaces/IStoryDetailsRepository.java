/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.repositories.sql.interfaces;

import com.thesiswork.model.StoryDetails;
import java.util.List;

/**
 *
 * @author keshant
 */
public interface IStoryDetailsRepository {
    
    public List<StoryDetails> getDetails();
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.repositories.sql;

import com.thesiswork.model.StoryDetails;
import com.thesiswork.repositories.sql.interfaces.IStoryDetailsRepository;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author keshant
 */
@Component
public class StoryDetailsRepository implements IStoryDetailsRepository {
    
    @PersistenceContext
    private EntityManager em;
    
    public List<StoryDetails> getDetails() {
        
//        List<StoryDetails> details = em.createNativeQuery("SELECT story.story_id AS storyId, story.description AS storyDesc, " +
//                                                            "characters.id AS characterId, characters.description AS characterDesc, characters.gender AS gender, " +
//                                                            "costumes.id AS costumeId, costumes.description AS costumeDesc " +
//                                                            "FROM story, characters, costumes " +
//                                                            "WHERE story.character_id = characters.id " +
//                                                            "AND characters.costume = costumes.id ORDER BY story.story_id", "StoryDetails").getResultList();
    	List<StoryDetails> details = em.createNativeQuery("SELECT storyId, storyString AS storyDesc, " +
                "characterId, characterSkin AS characterDesc, gender " +
                "FROM content_data ORDER BY storyId", "StoryDetails").getResultList();
        
        return details;
    }    
    
    public List<StoryDetails> getStoryIdString(String[] storyIds) {
    	
    	//String storyIdStr = String.join(",", storyIds);
    	
    	//return em.createNativeQuery("SELECT storyId, storyString FROM content_data WHERE storyId IN ("+storyIdStr+")", "StoryDetails").getResultList();
    	return null;
    	
    }
}

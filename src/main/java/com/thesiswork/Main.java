/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.thesiswork.model.StoryDetails;
import com.thesiswork.services.interfaces.IPublicVideoService;
import com.thesiswork.services.interfaces.IStoryDetailsService;
import com.thesiswork.services.interfaces.IUserClusteringService;
import com.thesiswork.services.interfaces.IUserProfilingService;

/**
 *
 * @author keshant
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Main {    
	
private static String CLUSTERED_VIDEOS_PATH = "data/videos";
	
	public static void main(String[] args) {
		ApplicationContext ctx = (ApplicationContext) SpringApplication.run(Main.class, args);
		IStoryDetailsService storyDetailsService = (IStoryDetailsService) ctx.getBean("storyDetailsService");
		IUserClusteringService userClusteringService = (IUserClusteringService) ctx.getBean("userClusteringService");
		IPublicVideoService publicVideoService = (IPublicVideoService) ctx.getBean("publicVideoService");
		IUserProfilingService userProfilingService = (IUserProfilingService) ctx.getBean("userProfilingService");
		
		List<String> features = storyDetailsService.getFeatures();
		List<StoryDetails> storyDetails = storyDetailsService.getStoryDetails();
		
		Map<String, String> clusterPath = new HashMap();
		clusterPath.put("sequenceFile", "data/user/inputs/users");
		clusterPath.put("initialClusterPath", "data/user/clusters/part-00000");
		clusterPath.put("inputs", "data/user/inputs");
		clusterPath.put("clusters", "data/user/clusters");
		clusterPath.put("output", "data/user/output");
						
		//userProfilingService.getUserProfiles("");		
		//userClusteringService.GenerateClusters(features, clusterPath);		
		//List<Map<String, List<String>>> clusters = userClusteringService.GetClusterBehavior(features, clusterPath);
		//publicVideoService.AssignToCluster(clusters, storyDetails, CLUSTERED_VIDEOS_PATH);
		//publicVideoService.UpdateLanguageSpecificRank();
		//publicVideoService.GetSimilarUserPreferences("cbe2c7e0-1126-4132-9fe7-8673b6447266");
		
		//PerformanceAnalysis(ctx);
		System.out.println("Done");
	}
	
	public static void PerformanceAnalysis(ApplicationContext ctx) {
		
		IPublicVideoService publicVideoService = (IPublicVideoService) ctx.getBean("publicVideoService");
			
		// Case 1 (basic) - 3 cases (500, 5000, 10000, 100000, 1000000)
		String userId = "2a887138-076d-4d45-975f-abd12f82b2ef";	//"85B2DA27-234E-4A86-978D-9D84F518AB1B";//	"";
		String language = "it";
		String filteredVideos = "";
		int records = 15;
		
		// Case 2 (filter - already watched, liked videos) - 3 cases
//		String userId = ""; //"d7804272-f71c-4cbe-8975-5b6d62950698";
//		String language = "en";
//		String filteredVideos = "T9XUOEmp7QWu0btqRCYXcmUdupw,e2aXGktWR6cRoVGCAlJtMS971R0,vSnDNCjy2Dcj_DQnHAneLPvXhqo,-367h-hhqLvv9cQsyayjt950pv0,8AVYzsWecwwA7QrVdbkzqirHzAw,"+
//				"-4UPKRz7mRdkU-tA9lAToncdDOA,G4ZHWzFZfs_PtloBAK9YHMQYjJs,poZAo9jg5u_QgUwPlkIjK4-d1QA,fw4xmmv19kJsJQZGALW6oeeYHm0,U_VpizTEEkVuGO8eaFntEvf3ZYs,"+
//				"9pJ1C_anRkhmkS3en2nQvqRXFuY,YU3AciOghlZgOdJRtDTiXEAD4os,8aDpSKxUdVu8th0wRGTuqS4lIXM,shQQvag9dr7rcMtkMA6SeQa7Cug,9wc7RqIC5w3EDK_u6SbyXzS3raI";
//		int records = 15;
		
		// Case 3 (item-based combined with user-based) - 1 case
				
		for(int i=0; i<5; i++) {
			
			long started, ended;			
			started = System.currentTimeMillis();			
			publicVideoService.RecommendToUser(userId, language, filteredVideos, records);
			ended = System.currentTimeMillis();
			System.out.println("Execution time for (Run "+i+"):" + (ended-started));					
		}
	}
}

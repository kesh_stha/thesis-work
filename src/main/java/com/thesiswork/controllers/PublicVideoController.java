/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.controllers;

import com.thesiswork.model.PublicVideo;
import com.thesiswork.services.interfaces.IPublicVideoService;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author keshant
 */
@RestController
@RequestMapping("/videos/public")   
public class PublicVideoController {
   
    @Autowired
    private IPublicVideoService _publicVideoService;
    @Autowired
       
    public @ResponseBody Page<PublicVideo> getVideos() {
        
        return this._publicVideoService.getVideos(1, 10);
    }
    
    @RequestMapping("/{page}/{size}")    
    public Page<PublicVideo> getVideos(@PathVariable(value = "page") int page, @PathVariable (value = "size") int size) {
        
        return this._publicVideoService.getVideos(page, size);
    }
    
    @RequestMapping("/{bundle}")    
    public List<PublicVideo> getVideosByMetaBundle(@PathVariable(value = "bundle") String bundle) {
        
        return this._publicVideoService.getVideosByMetaBundleString(bundle);
    }
    
    @RequestMapping(value = "/recommendations", method = RequestMethod.POST)
    public List<PublicVideo> getRecommendedVideos(@RequestBody String json) {
    	JSONObject requestBody = new JSONObject(json);
    	String userId = requestBody.getString("userId");
    	String language = requestBody.getString("language");
    	String filteredVideos = requestBody.getString("filteredVideos");    	
    	int records = requestBody.getInt("records");
    	return this._publicVideoService.RecommendToUser(userId, language, filteredVideos, records);
    }
    
}

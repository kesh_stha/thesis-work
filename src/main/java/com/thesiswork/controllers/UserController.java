/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.controllers;


import com.thesiswork.model.Profile;
import com.thesiswork.services.interfaces.IProfileService;
import com.thesiswork.services.interfaces.IVideoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author keshant
 */
@RestController
public class UserController {

    
    @Autowired
    private IProfileService _profileService;
    //private IVideoService _videoService;
    
    //@RequestMapping("/user/{uuid}")
//    public long getLastActiveTimeOfUser(@PathVariable(value = "uuid")String uuid) {
//        return _videoService.getLastActiveTimeOfUser(uuid);
//    }
    @RequestMapping("/user/{uuid}")    
    public Profile getUserDetails(@PathVariable(value="uuid") String userId){
    	
    	return _profileService.getUserDetails(userId);
    }
    
}

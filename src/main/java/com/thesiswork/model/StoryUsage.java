package com.thesiswork.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author keshant
 */
@Document(collection = "story_usage")
public class StoryUsage {

	@Id
    private Object id;
    private Value value;

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }
        
    public class Value {
                
        private String language;
        private int story;
        private int count;
        
        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public int getStory() {
            return story;
        }

        public void setStory(int story) {
            this.story = story;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }        
    }
}

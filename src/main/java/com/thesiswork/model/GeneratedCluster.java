package com.thesiswork.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "generated_cluster")
public class GeneratedCluster {
	
	@Id
	private String id;
	private List<String> members;
	private List<String> behavior;
	//private List<PublicVideo> matchedVideos;
	private byte[] matchedVideos;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<String> getMembers() {
		return members;
	}
	public void setMembers(List<String> members) {
		this.members = members;
	}
	public List<String> getBehavior() {
		return behavior;
	}
	public void setBehavior(List<String> behavior) {
		this.behavior = behavior;
	}	
		
	public byte[] getMatchedVideos() {
		return matchedVideos;
	}
	public void setMatchedVideos(byte[] matchedVideos) {
		this.matchedVideos = matchedVideos;
	}
}

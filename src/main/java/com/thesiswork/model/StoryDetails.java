/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

/**
 *
 * @author keshant
 */

@SqlResultSetMapping(
        name = "StoryDetails",
        entities = {
            @EntityResult(
                entityClass = StoryDetails.class,
                fields = {
                    @FieldResult(name = "storyId", column = "storyId"),
                    @FieldResult(name = "storyDesc", column = "storyDesc"),
                    @FieldResult(name = "characterId", column = "characterId"),
                    @FieldResult(name = "characterDesc", column = "characterDesc"),
                    @FieldResult(name = "gender", column = "gender")
                })
        }
)
@Entity
public class StoryDetails implements Serializable {
    
    @Id
    private int storyId;
    private String storyDesc;
    private int characterId;
    private String characterDesc;    
    private String gender;

    public int getStoryId() {
        return storyId;
    }

    public void setStoryId(int storyId) {
        this.storyId = storyId;
    }

    public String getStoryDesc() {
        return storyDesc;
    }

    public void setStoryDesc(String storyDesc) {
        this.storyDesc = storyDesc;
    }

    public int getCharacterId() {
        return characterId;
    }

    public void setCharacterId(int characterId) {
        this.characterId = characterId;
    }

    public String getCharacterDesc() {
        return characterDesc;
    }

    public void setCharacterDesc(String characterDesc) {
        this.characterDesc = characterDesc;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
}

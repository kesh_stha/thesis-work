/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.model;

import java.util.Date;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 *
 * @author keshant
 */
@Document(collection = "video_public")
public class PublicVideo {
    
	@Id
    private String id;
    private String title;
    private String categoryId;
    private List<CategoryId> categoryIds;
    private String uuid;
    private Date createdAt;
    private long publishedAt;
    private long approvedAt;   
    private String language;
    private Urls urls;
    private Meta meta;
    private boolean published;
    private boolean approved;
    private int nbrOfLikes;
    private int nbrOfViews;
    private int nbrOfDownloads;
    private int nbrOfShares;
    private int nbrOfReports;
    private int editorWeight;
    private int rank;    
    private String islandId;
    private int score;
    private int ldRank;
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getId(){
        return id;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setCategoryId(String categoryId){
        this.categoryId = categoryId;
    }
    
    public String getCategoryId(){
        return categoryId;
    }
    
    public void setCategoryIds(List<CategoryId> categoryIds) {
        this.categoryIds = categoryIds;
    }
    
    public List<CategoryId> getCategoryIds() {
        return categoryIds;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public long getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(long publishedAt) {
        this.publishedAt = publishedAt;
    }

    public long getApprovedAt() {
        return approvedAt;
    }

    public void setApprovedAt(long approvedAt) {
        this.approvedAt = approvedAt;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Urls getUrls() {
        return urls;
    }

    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public int getNbrOfLikes() {
        return nbrOfLikes;
    }

    public void setNbrOfLikes(int nbrOfLikes) {
        this.nbrOfLikes = nbrOfLikes;
    }

    public int getNbrOfViews() {
        return nbrOfViews;
    }

    public void setNbrOfViews(int nbrOfViews) {
        this.nbrOfViews = nbrOfViews;
    }

    public int getNbrOfDownloads() {
        return nbrOfDownloads;
    }

    public void setNbrOfDownloads(int nbrOfDownloads) {
        this.nbrOfDownloads = nbrOfDownloads;
    }

    public int getNbrOfShares() {
        return nbrOfShares;
    }

    public void setNbrOfShares(int nbrOfShares) {
        this.nbrOfShares = nbrOfShares;
    }

    public int getNbrOfReports() {
        return nbrOfReports;
    }

    public void setNbrOfReports(int nbrOfReports) {
        this.nbrOfReports = nbrOfReports;
    }

    public int getEditorWeight() {
        return editorWeight;
    }

    public void setEditorWeight(int editorWeight) {
        this.editorWeight = editorWeight;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getIslandId() {
        return islandId;
    }

    public void setIslandId(String islandId) {
        this.islandId = islandId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
    
    public void setLdRank(int ldRank) {
        this.ldRank = ldRank;
    }
    
    public int getLdRank(){
        return ldRank;
    }
    
    public class CategoryId {
        private String categoryId;
        private long publishedAt;

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public long getPublishedAt() {
            return publishedAt;
        }

        public void setPublishedAt(long publishedAt) {
            this.publishedAt = publishedAt;
        }
    }
    
    public class Urls {
        private String web;
        private String file;
        private String charImg;
        private String bgImg;

        public String getWeb() {
            return web;
        }

        public void setWeb(String web) {
            this.web = web;
        }

        public String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }

        public String getCharImg() {
            return charImg;
        }

        public void setCharImg(String charImg) {
            this.charImg = charImg;
        }

        public String getBgImg() {
            return bgImg;
        }

        public void setBgImg(String bgImg) {
            this.bgImg = bgImg;
        }        
    }
    
    public class Meta {
        private int bundle;
        private int story;
        private int stage;
        private String bundleString;
        private String storyString;  

        public int getBundle() {
            return bundle;
        }

        public void setBundle(int bundle) {
            this.bundle = bundle;
        }

        public int getStory() {
            return story;
        }

        public void setStory(int story) {
            this.story = story;
        }

        public int getStage() {
            return stage;
        }

        public void setStage(int stage) {
            this.stage = stage;
        }

        public String getBundleString() {
            return bundleString;
        }

        public void setBundleString(String bundleString) {
            this.bundleString = bundleString;
        }

        public String getStoryString() {
            return storyString;
        }

        public void setStoryString(String storyString) {
            this.storyString = storyString;
        }        
    }
}

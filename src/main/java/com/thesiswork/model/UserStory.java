/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author keshant
 */
@Document(collection = "user_story")
public class UserStory {
    
	@Id
    private Object id;
    private Value value;

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }
        
    public class Value {
        
        private String user;
        private String language;
        private int story;
        private int count;

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public int getStory() {
            return story;
        }

        public void setStory(int story) {
            this.story = story;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }        
    }    
}

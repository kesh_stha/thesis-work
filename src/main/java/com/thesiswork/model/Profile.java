/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.model;
/**
 *
 * @author keshant
 */
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author keshant
 */
@Document(collection = "users")
public class Profile {
    
	@Id
    private String userId;
    private String gender;    
    private String language;
    private List<PreferredLanguage> preferredLanguage;
    private List<FavoriteStory> favStories;
    private List<FavoriteCharacter> favCharacters;    
    private String filteredVideos;
    private long activeTime;
    private List<String> similarPreferences;
        
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    
    public List<PreferredLanguage> getPreferredLanguage() {
        return preferredLanguage;
    }

    public void setPreferredLanguage(List<PreferredLanguage> preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }
    
    public List<FavoriteStory> getFavStories() {
        return favStories;
    }

    public void setFavStories(List<FavoriteStory> favStories) {
        this.favStories = favStories;
    }

    public List<FavoriteCharacter> getFavCharacters() {
        return favCharacters;
    }

    public void setFavCharacters(List<FavoriteCharacter> favCharacters) {
        this.favCharacters = favCharacters;
    }
        
    public String getFilteredVideos() {
        return filteredVideos;
    }

    public void setFilteredVideos(String filteredVideos) {
        this.filteredVideos = filteredVideos;
    }
    
    public long getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(long activeTime) {
        this.activeTime = activeTime;
    }
    
    public List<String> getSimilarPreferences() {
    	return similarPreferences;
	}
    
    public void setSimilarPreferences(List<String> similarPreferences){
    	this.similarPreferences = similarPreferences;
    }
    
    public class PreferredLanguage {
    
        private String language;
        private int count;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
        
    }
    public class FavoriteStory {
        
        private long storyId;
        private int count;

        public long getStoryId() {
            return storyId;
        }

        public void setStoryId(long storyId) {
            this.storyId = storyId;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }        
    }
    
    public class FavoriteCharacter {
        
        private long characterId;
        private int count;

        public long getCharacterId() {
            return characterId;
        }

        public void setCharacterId(long characterId) {
            this.characterId = characterId;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
        
    }   
}

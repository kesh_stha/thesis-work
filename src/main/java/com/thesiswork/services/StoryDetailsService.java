/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.services;

import com.thesiswork.model.StoryDetails;
import com.thesiswork.repositories.sql.interfaces.IStoryDetailsRepository;
import com.thesiswork.services.interfaces.IStoryDetailsService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author keshant
 */
@Service
public class StoryDetailsService implements IStoryDetailsService {
    
    @Autowired
    private IStoryDetailsRepository _storyDetailsRepository;

    @Override
    public List<String> getFeatures() {
        
        List<String> features = new ArrayList<String>();
        
        List<StoryDetails> storyDetails = _storyDetailsRepository.getDetails();
        
        for(StoryDetails detail : storyDetails) {
            features.add("story_" + detail.getStoryId());
            if(!features.contains("character_" + detail.getCharacterId())) {
                features.add("character_" + detail.getCharacterId());
            }
        }
        
        Collections.sort(features);
        features.add("male");
        features.add("female");
        features.add("unknown");
        
        return features;
    }
    
    @Override
    public List<StoryDetails> getStoryDetails() {
    	
    	return _storyDetailsRepository.getDetails();
    }
    
}

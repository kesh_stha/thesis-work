/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.services;

import com.thesiswork.model.Profile;
import com.thesiswork.repositories.mongo.ProfileRepository;
import com.thesiswork.services.interfaces.IUserClusteringService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configuration;
import org.apache.mahout.clustering.Cluster;
import org.apache.mahout.clustering.classify.WeightedPropertyVectorWritable;
import org.apache.mahout.clustering.kmeans.KMeansDriver;
import org.apache.mahout.clustering.kmeans.Kluster;
import org.apache.mahout.common.HadoopUtil;
import org.apache.mahout.common.distance.TanimotoDistanceMeasure;
import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.Vector;
import org.apache.mahout.math.VectorWritable;
import org.apache.mahout.utils.clustering.ClusterDumper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author keshant
 */
@Service
public class UserClusteringService implements IUserClusteringService {    
    
    final static Configuration conf = new Configuration();
    
    @Autowired
    private ProfileRepository _profileRepository;
    
    
    public void GenerateClusters(List<String> features, Map<String, String> filePath) {
        
        List<NamedVector> featureVector = new ArrayList<NamedVector>();
                
        Page<Profile> userProfiles = _profileRepository.findAll(new PageRequest(0, 1500));
        int pageNumbers = userProfiles.getTotalPages();
        int pageNum = 0;
        while(pageNumbers >= pageNum) {
        	if(pageNum > 0) {
        		userProfiles = _profileRepository.findAll(new PageRequest(pageNum, 1500));
        	}
        	for(Profile profile : userProfiles) {
            	featureVector.add(CreateUserVector(profile, features));
            }        	
        	pageNum++;
        	System.out.println("Memory1: " + Runtime.getRuntime().freeMemory() + ", Page: " + pageNum);  
        	
        }      

        Runtime.getRuntime().gc();
    	System.out.println("Memory2: " + Runtime.getRuntime().freeMemory());  
    	
        long totalUsers = _profileRepository.count();
        int k = (int) Math.sqrt(totalUsers/2);
        CreateSequenceFile(featureVector, filePath.get("sequenceFile")); // "data/user/inputs/users");
        CreateInitialClusters(featureVector.subList(0, k), filePath.get("initialClusterPath")); // "data/user/clusters/part-00000");
        featureVector.clear();
        ClusterData(filePath.get("inputs"), filePath.get("clusters"), filePath.get("output")); // "data/user/inputs", "data/user/clusters", "data/user/output");
    }
    
    public List<Map<String, List<String>>> GetClusterBehavior(List<String> features, Map<String, String> filePath) {   
        
        Map<Integer,List<WeightedPropertyVectorWritable>> result = ReadClusterData(filePath.get("output")+"/"+Cluster.CLUSTERED_POINTS_DIR);
        // get features
        //List<String> features = _storyRepo.GetFeatures();
        List<Map<String, List<String>>> clusters = new ArrayList<Map<String, List<String>>>();        
        for(Map.Entry<Integer, List<WeightedPropertyVectorWritable>> data : result.entrySet()) {            
            
            List<WeightedPropertyVectorWritable> nv = data.getValue();
            List<String> clusterMembers = new ArrayList<String>();
            List<String> clusterBehavior = new ArrayList<String>();
            for(WeightedPropertyVectorWritable v : nv) {
                Vector clusterData = v.getVector();
                String[] clusterDataToArray = clusterData.toString().split(":", 2);
                clusterMembers.add(clusterDataToArray[0].replaceAll("_", "-"));
                
                String[] clusterBehaviorArray = clusterDataToArray[1].replaceAll("[{}]", "").replaceAll(":1.0", "").split(",");
                for(String attributeIndex : clusterBehaviorArray) {
                    String featureValue = features.get(Integer.parseInt(attributeIndex));
                    if(!clusterBehavior.contains(featureValue)) {                        
                        clusterBehavior.add(featureValue);
                    }
                }                
            }    
            
            System.out.println("Read cluster Memory: " + Runtime.getRuntime().freeMemory());  
            
            Map<String, List<String>> cluster = new HashMap<String, List<String>>();
            cluster.put("members", clusterMembers);
            cluster.put("behavior", clusterBehavior);
            clusters.add(cluster);            
        }
        
        return clusters;
    }
    
    private Map<Integer,List<WeightedPropertyVectorWritable>> ReadClusterData(String filePath) {
        
        ClusterDumper cdump = new ClusterDumper();
        return ClusterDumper.readPoints(new Path(filePath), cdump.getMaxPointsPerCluster(), conf);        
    }
    
    private void CreateSequenceFile(List<NamedVector> vectors, String filePath){
        
        try {            
            Path path = new Path(filePath);       
            
            SequenceFile.Writer.Option[] options = {
                SequenceFile.Writer.file(path),
                SequenceFile.Writer.keyClass(Text.class),
                SequenceFile.Writer.valueClass(VectorWritable.class)
            };
            
            SequenceFile.Writer writer = SequenceFile.createWriter(conf, options);
            
            VectorWritable vec = new VectorWritable();
            for(NamedVector v : vectors) {                
                if(v != null) {
                	vec.set(v);
                	writer.append(new Text(v.getName()), vec);
                }
                else {
                	System.out.println(v);
                }
                System.out.println("SeqFile Memory: " + Runtime.getRuntime().freeMemory());  
            }
            
            writer.close();            
        } 
        catch (IOException ex) {
            Logger.getLogger(UserClusteringService.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    private void CreateInitialClusters(List<NamedVector> vectors, String filePath){
        
        try {            
            Path path = new Path(filePath);
            SequenceFile.Writer.Option[] options = {
                SequenceFile.Writer.file(path),
                SequenceFile.Writer.keyClass(Text.class),
                SequenceFile.Writer.valueClass(Kluster.class)
            };
            
            SequenceFile.Writer writer = SequenceFile.createWriter(conf, options);
            
            int clusterNum = 0;
            for(NamedVector v : vectors) {
                Kluster cluster = new Kluster(v, clusterNum++, new TanimotoDistanceMeasure());
                writer.append(new Text(v.getName()), cluster);
                System.out.println("Initial Cluster Memory: " + Runtime.getRuntime().freeMemory());  
            }
            
            writer.close();
        }
        catch(IOException ex) {
            Logger.getLogger(UserClusteringService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void ClusterData(String inputPath, String initialClusters, String outputPath) {
        
        try {
            HadoopUtil.delete(conf, new Path(outputPath));
            KMeansDriver.run(conf, new Path(inputPath), new Path(initialClusters), new Path(outputPath), 0.001, 20, true, 0, true);
        } 
        catch (IOException ex) {
            Logger.getLogger(UserClusteringService.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (InterruptedException ex) {
            Logger.getLogger(UserClusteringService.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(UserClusteringService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private NamedVector CreateUserVector(Profile userProfile, List<String> features) {
        try {
            double[] videoFeatureValue = new double[features.size()];
            
            videoFeatureValue[features.indexOf(userProfile.getGender().toLowerCase())] = 1;

            List<Profile.FavoriteStory> favStories = userProfile.getFavStories();
            if(favStories != null){
                for(Profile.FavoriteStory story : favStories){
                    videoFeatureValue[features.indexOf("story_" + story.getStoryId())] = 1;
                }
            }

            List<Profile.FavoriteCharacter> favCharacters = userProfile.getFavCharacters();
            if(favCharacters != null) {
                for(Profile.FavoriteCharacter character : favCharacters){
                    videoFeatureValue[features.indexOf("character_" + character.getCharacterId())] = 1;
                }
            }
            
            return new NamedVector(new DenseVector(videoFeatureValue), userProfile.getUserId().replaceAll("-", "_"));
        }
        catch(Exception ex) {
            System.out.println(userProfile.getUserId().replaceAll("-", "_"));
            return null;
        }
    }
    
}

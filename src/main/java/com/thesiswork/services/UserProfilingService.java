/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.services;

import com.google.gson.Gson;
import com.thesiswork.model.Profile;
import com.thesiswork.model.UserStory;
import com.thesiswork.model.StoryDetails;
import com.thesiswork.repositories.mongo.ProfileRepository;
import com.thesiswork.repositories.mongo.UserStoryRepository;
import com.thesiswork.repositories.sql.interfaces.IStoryDetailsRepository;
import com.thesiswork.services.interfaces.IUserProfilingService;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.neo4j.cypher.internal.compiler.v2_1.perty.docbuilders.toStringDocBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author keshant
 */
@Service
public class UserProfilingService implements IUserProfilingService {

    @Autowired
    private IStoryDetailsRepository _storyDetailsRepository;
    @Autowired
    private UserStoryRepository _userStoryRepository;
    @Autowired
    private ProfileRepository _profileRepository;
    
    @Override
    public void getUserProfiles(String userProfilePath) {
        // initialize variables
        UserStory prevUserProfile = new UserStory();
        List<Profile> userProfileList = new ArrayList<Profile>();
        int count = 0;
        // Get story details from MySQL DB           
        List<StoryDetails> storyDetails = _storyDetailsRepository.getDetails();
        // Get user story combination
        Page<UserStory> pagedUserStory = _userStoryRepository.findAll(new PageRequest(0, 1500, new Sort(Sort.Direction.DESC, "value.user")));
        
        //List<UserStory> userStory = _userStoryRepository.findAll(new Sort(Sort.Direction.DESC, "value.user"));
        
        int totalPages = pagedUserStory.getTotalPages();
        int pageNum = 0;
    	
        while(totalPages >= pageNum) {
        	
        	if(pageNum > 0) {
        		pagedUserStory = _userStoryRepository.findAll(new PageRequest(pageNum, 1500, new Sort(Sort.Direction.DESC, "value.user")));
        	}   
        	
        	for(UserStory us : pagedUserStory) {
                
                UserStory.Value curUserProfile = us.getValue();
                if(prevUserProfile.getValue() == null) {
                    prevUserProfile.setValue(curUserProfile);
                }            
                // check if profiling for current user is complete
                if(!prevUserProfile.getValue().getUser().equals(curUserProfile.getUser())) { // || !userStoryCombination.hasNext()) {     
                    // create profile of user and move to next one
                    Profile userProfile = CreateUserProfile(prevUserProfile.getValue());
                    userProfileList.add(userProfile);                  
                    count++;
                    prevUserProfile.setValue(curUserProfile);
                    ClearGlobals();
                }
                
                final long userVideoStory = curUserProfile.getStory();                            
                Collection userVideoStoryDetailsMatch = CollectionUtils.select(storyDetails, new Predicate() {
                    public boolean evaluate(Object userStoryDetails) {
                        return ((StoryDetails) userStoryDetails).getStoryId() == userVideoStory;
                    }
                });
                if(userVideoStoryDetailsMatch.size() > 0) {
                    StoryDetails userVideoStoryDetails = (StoryDetails)(userVideoStoryDetailsMatch.toArray())[0];
                    // Calculate user gender probability based on characters used
                    GetGenderProbability(userVideoStoryDetails.getGender());                    
                    // Get user stories usage
                    int storyUsedCount = curUserProfile.getCount(); 
                    GetUserStories(userVideoStory, storyUsedCount);
                    // Get user characters usage
                    GetUserCharacters(userVideoStoryDetails, storyUsedCount);
                    // Get user languages
                    GetUserLanguages(curUserProfile, storyUsedCount);
                }
            }
        	pageNum++; 
        	
        	//System.out.println("Page number: " + pageNum);
        	if(pageNum%500 == 0) {
        		_profileRepository.save(userProfileList);
        		userProfileList.clear();
        		System.out.println("Users: " + count);
        	}
        }
        
        if(!userProfileList.isEmpty()) {
        	_profileRepository.save(userProfileList);
        }
        
        System.out.println("Users: " + count);
        
    }
    
    // Global variables
    private static int gMaleProbability = 0;
    private static String gLanguage = "en";    
    private static Map<Long, Integer> gUserCharacters = new HashMap<Long, Integer>();            
    private static int gMaxCharacterUsed = 0;
    private static Map<Long, Integer> gUserStories = new HashMap<Long, Integer>();
    private static int gMaxStoryUsed = 0;
    private static Map<String, Integer> gUserLanguages = new HashMap<String, Integer>();
    private static int gMaxLanguageUsed = 0;
    
    private Profile CreateUserProfile(UserStory.Value profileObject) {
        
        Profile userProfile = new Profile();
        String gender = PredictGenderOfUser();
        List<Profile.PreferredLanguage> preferredLanguages = PredictPreferredLanguagesOfUser();
        List<Profile.FavoriteStory> favStories = PredictFavStoriesOfUser();
        List<Profile.FavoriteCharacter> favCharacters = PredictFavCharactersOfUser();
        
        if(profileObject != null) {
           
            userProfile.setUserId(profileObject.getUser());
            userProfile.setPreferredLanguage(preferredLanguages);
            userProfile.setLanguage(gLanguage);
            userProfile.setGender(gender);
            userProfile.setFavStories(favStories);
            userProfile.setFavCharacters(favCharacters);
        }
        return userProfile;        
    }
    
    private Map<Long, Integer> GetUserCharacters(StoryDetails userVideoStoryDetails, int totalCharactersUsedCount){
        // Get user characters usage
        long characterFromUserStory = userVideoStoryDetails.getCharacterId();                    
        
        if (gUserCharacters.containsKey(characterFromUserStory)){
            int initialCharacterCount = gUserCharacters.get(characterFromUserStory);
            totalCharactersUsedCount += initialCharacterCount;
            gUserCharacters.put(characterFromUserStory, totalCharactersUsedCount);
        }
        else {
            gUserCharacters.put(characterFromUserStory, totalCharactersUsedCount);
        }

        if(totalCharactersUsedCount > gMaxCharacterUsed) {
            gMaxCharacterUsed = totalCharactersUsedCount;
        }
        
        return gUserCharacters;
    }
    
    private Map<Long, Integer> GetUserStories(Long userVideoStory, int totalStoryUsedCount) {                
        
        if(gUserStories.containsKey(userVideoStory)) {
            int initialStoryCount = gUserStories.get(userVideoStory);
            totalStoryUsedCount += initialStoryCount;
            gUserStories.put(userVideoStory, totalStoryUsedCount);
        }
        else {
            gUserStories.put(userVideoStory, totalStoryUsedCount);
        }

        if(gMaxStoryUsed < totalStoryUsedCount) {
            gMaxStoryUsed = totalStoryUsedCount;
        }
        
        return gUserStories;
    }
    
    private Map<String, Integer> GetUserLanguages(UserStory.Value userProfile, int totalLanguageUsedCount){
                
        // Get user languages
        String userVideoLanguage = userProfile.getLanguage();            
        if(userVideoLanguage.matches("(.*)_(.*)")) {
            userVideoLanguage = userVideoLanguage.split("_")[0];
        }
        if(gUserLanguages.containsKey(userVideoLanguage)) {
            int initialLanguageCount = gUserLanguages.get(userVideoLanguage);
            totalLanguageUsedCount += initialLanguageCount;
            gUserLanguages.put(userVideoLanguage, totalLanguageUsedCount);
        }
        else {
            gUserLanguages.put(userVideoLanguage, totalLanguageUsedCount);
        }
        if(gMaxLanguageUsed < totalLanguageUsedCount) {
            gMaxLanguageUsed = totalLanguageUsedCount;
            gLanguage = userVideoLanguage;
        }
                
        return gUserLanguages;
    }
    
    private List<Profile.FavoriteCharacter> PredictFavCharactersOfUser() {
        
        List<Profile.FavoriteCharacter> favCharacters = new ArrayList<Profile.FavoriteCharacter>();
        if(gUserCharacters != null) {
            for (Map.Entry<Long, Integer> userCharacter : gUserCharacters.entrySet()) {
                long characterId = userCharacter.getKey();
                int used = userCharacter.getValue();
                double relativeRatio = (double)used/gMaxCharacterUsed;
                
                //if(relativeRatio > 0.5) {
                    Profile.FavoriteCharacter favCharacter = new Profile().new FavoriteCharacter();
                    favCharacter.setCharacterId(characterId);
                    favCharacter.setCount(used);
                    favCharacters.add(favCharacter);
                //}
            }
        }
        return favCharacters;
    } 
    
    private List<Profile.FavoriteStory> PredictFavStoriesOfUser() {
        
        List<Profile.FavoriteStory> favStories = new ArrayList<Profile.FavoriteStory>();
        if(gUserStories != null) {
            for (Map.Entry<Long, Integer> userStory : gUserStories.entrySet()) {
                long storyId = userStory.getKey();
                int used = userStory.getValue();
                double relativeRatio = (double)used/gMaxStoryUsed;
                
                //if(relativeRatio > 0.50) {
                    Profile.FavoriteStory favStory = new Profile().new FavoriteStory();
                    favStory.setStoryId(storyId);
                    favStory.setCount(used);
                    favStories.add(favStory);
                //}
            }
        }
        return favStories;
    }
    
    private List<Profile.PreferredLanguage> PredictPreferredLanguagesOfUser() {
        
        List<Profile.PreferredLanguage> preferredLanguages = new ArrayList<Profile.PreferredLanguage>();
        if(gUserLanguages != null) {
            for (Map.Entry<String, Integer> userLanguage : gUserLanguages.entrySet()) {
                String language = userLanguage.getKey();
                int used = userLanguage.getValue();
                double relativeRatio = (double)used/gMaxLanguageUsed;
                
                //if(relativeRatio > 0.50) {
                    Profile.PreferredLanguage preferredLanguage = new Profile().new PreferredLanguage();
                    preferredLanguage.setLanguage(language);
                    preferredLanguage.setCount(used);
                    preferredLanguages.add(preferredLanguage);
                //}
            }
        }
        return preferredLanguages;
    }
    
    private int GetGenderProbability (String storyGender) {
        
        try {
            if(storyGender.equalsIgnoreCase("male")) {
                gMaleProbability++;                
            }
            else {
                gMaleProbability--;
            }
        }
        catch (Exception ex) {}        
                
        return gMaleProbability;
    }
    
    private String PredictGenderOfUser() {
                
        if(gMaleProbability > 0) {
            return "male";
        }
        else if(gMaleProbability < 0) {
            return "female";
        }
        return "Unknown";
    }
        
    private void ClearGlobals() {
        
        gMaleProbability = 0;    
        gLanguage = "en";
        gUserCharacters = new HashMap<Long, Integer>();
        gMaxCharacterUsed = 0;
        gUserStories = new HashMap<Long, Integer>();
        gMaxStoryUsed = 0;        
        gUserLanguages = new HashMap<String, Integer>();
        gMaxLanguageUsed = 0;
    }
    
}

package com.thesiswork.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thesiswork.model.Profile;
import com.thesiswork.repositories.mongo.ProfileRepository;
import com.thesiswork.services.interfaces.IProfileService;

@Service
public class ProfileService implements IProfileService {
	
	@Autowired
    private ProfileRepository _profileRepository;
	
	@Override
	public Profile getUserDetails(String userId) {
		
		return _profileRepository.findByUserId(userId);
	}

}

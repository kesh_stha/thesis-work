/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.services;

import com.thesiswork.model.Video;
import com.thesiswork.repositories.mongo.VideoRepository;
import com.thesiswork.services.interfaces.IVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author keshant
 */
@Service
public class VideoService implements IVideoService {

    @Autowired
    private VideoRepository _videoRepository;
    
    @Override
    public long getLastActiveTimeOfUser(String uuid) {
        
        Page<Video> temp = _videoRepository.findByUserUuid(uuid, new PageRequest(1, 1));
        
        //Video result = _videoRepository.findOneByUuid(uuid, new PageRequest(1, 1, new Sort(Sort.Direction.DESC, "timestamp"))).getContent().get(0);
        return 2;
    }
    
}

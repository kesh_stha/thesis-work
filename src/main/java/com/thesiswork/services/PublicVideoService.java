/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.services;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.thesiswork.model.GeneratedCluster;
import com.thesiswork.model.Profile;
import com.thesiswork.model.PublicVideo;
import com.thesiswork.model.StoryDetails;
import com.thesiswork.model.StoryUsage;
import com.thesiswork.repositories.mongo.GeneratedClusterRepository;
import com.thesiswork.repositories.mongo.ProfileRepository;
import com.thesiswork.repositories.mongo.PublicVideoRepository;
import com.thesiswork.repositories.mongo.StoryUsageRepository;
import com.thesiswork.repositories.mongo.VideoRepository;
import com.thesiswork.repositories.sql.StoryDetailsRepository;
import com.thesiswork.services.interfaces.IPublicVideoService;

/**
 *
 * @author keshant
 */
@Service
public class PublicVideoService implements IPublicVideoService {
    
    @Autowired
    private PublicVideoRepository _publicVideoRepository;
    @Autowired
    private StoryDetailsRepository _storyDetailsRepository;
    @Autowired
    private GeneratedClusterRepository _generatedClusterRepository;
    @Autowired
    private ProfileRepository _profileRepository;
    @Autowired
    private StoryUsageRepository _storyUsageRepository;
    @Autowired
    private VideoRepository _videoRepository;
   
    @Override
    public Page<PublicVideo> getVideos(int page, int size) {
        
        Page<PublicVideo> videos = this._publicVideoRepository.findAll(new PageRequest(page, size));
        return videos;        
    }
    
    @Override
    public List<PublicVideo> getVideosByMetaBundleString(String bundleString) {
        
        List<PublicVideo> videos = this._publicVideoRepository.findByMetaBundleString(bundleString);
        return videos;
    }
    
    @Override
    public List<PublicVideo> getVideosByTitleRegex(String title) {
        List<PublicVideo> videos = this._publicVideoRepository.findByTitleRegex(title);
        return videos;
    }
    
    @Override
    public void AssignToCluster(List<Map<String, List<String>>> clusters, List<StoryDetails> storyDetails, String clusteredVideosPath) {
    	
    	Runtime.getRuntime().gc();
    	System.out.println("Assign to cluster cluster Memory: " + Runtime.getRuntime().freeMemory());  
    	_generatedClusterRepository.deleteAll();
        
        //List<StoryDetails> storyDetails = _storyDetailsRepository.getDetails();
        
        int clusterNum = 0;
        for (Map<String, List<String>> cluster : clusters) {
            
            System.out.println("\nCluster " + clusterNum);
            System.out.println("---------------------------------");
            
            List<String> clusterBehavior = cluster.get("behavior");
            List<String> clusterMembers = cluster.get("members");
            
            Page<PublicVideo> pagedVideos = _publicVideoRepository.findAll(new PageRequest(0, 1500));
            List<PublicVideo> matchedVideos = MatchVideos(pagedVideos.getContent(), storyDetails, clusterBehavior, null);
            int totalPages = pagedVideos.getTotalPages();
            int pageNum = 0;
            while(totalPages >= pageNum) {
            	if(pageNum > 0) {
            		pagedVideos = _publicVideoRepository.findAll(new PageRequest(pageNum, 1500));
            		matchedVideos.addAll(MatchVideos(pagedVideos.getContent(), storyDetails, clusterBehavior, null));
            	}            	
            	pageNum++;
            }
            
            GeneratedCluster generatedCluster = new GeneratedCluster();
            generatedCluster.setId("cluster_" + clusterNum);
            generatedCluster.setMembers(clusterMembers);
            generatedCluster.setBehavior(clusterBehavior);
            generatedCluster.setMatchedVideos(Compress(new Gson().toJson(matchedVideos)));
            
            _generatedClusterRepository.save(generatedCluster); 
              
            // next cluster
            clusterNum++;
        }
        
    }
    
    public void UpdateLanguageSpecificRank() {
    	
    	String[] languages = new String[] {"en","de", "it", "th", "ru"};
    	for(String language : languages) {
    		List<StoryUsage> storyByLanguage = _storyUsageRepository.findByValueLanguage(language, new Sort(Direction.DESC, "value.count"));
    		if(storyByLanguage.size() <= 0) {
    			continue;
    		}
        	int maxStoryUsage = storyByLanguage.get(0).getValue().getCount();
        	
        	Page<PublicVideo> pagedVideos = _publicVideoRepository.findByLanguage(language, new PageRequest(0, 500));
        	int totalPages = pagedVideos.getTotalPages();
            int pageNum = 0;
        	
            while(totalPages >= pageNum) {
            	
            	if(pageNum > 0) {
            		pagedVideos = _publicVideoRepository.findByLanguage(language, new PageRequest(pageNum, 500));	
            	}         
            	for(PublicVideo video : pagedVideos) {
            		long rank = video.getNbrOfLikes() + 
            				video.getNbrOfViews() +
            				video.getNbrOfDownloads() + 
            				video.getNbrOfShares() + 
            				video.getNbrOfViews();
            		
            		// binary search
            		boolean matchStory = false;
            		int storyCount = 0;
            		List<StoryUsage> tempStoryByLanguage = storyByLanguage;
            		while(!matchStory) {   
            			int listSize = tempStoryByLanguage.size();    		
            			if(listSize <= 5) {
            				for(StoryUsage storyUsage : tempStoryByLanguage) {
            					if(storyUsage.getValue().getStory() == video.getMeta().getStory()) {
            						storyCount = storyUsage.getValue().getCount();
            						matchStory = true;
            						break;
            					}
            				}
            				break;
            			}
            			
            			int listCenterIndex = Math.round(listSize/2);   
            			List<StoryUsage> listLeft = tempStoryByLanguage.subList(0, listCenterIndex-1);    		
            			List<StoryUsage> listRight = tempStoryByLanguage.subList(listCenterIndex+1, listSize-1);
            			StoryUsage.Value listCenter = tempStoryByLanguage.get(listCenterIndex).getValue();
            			
            			if(video.getMeta().getStory() ==  listCenter.getStory()) {
            				storyCount = listCenter.getCount();
            				matchStory = true;
            			}
            			else if (video.getMeta().getStory() > listCenter.getStory()) {
            				tempStoryByLanguage = listLeft;
            			}
            			else if (video.getMeta().getStory() < listCenter.getStory()) {
            				tempStoryByLanguage = listRight;
            			}
            			else {
            				matchStory = true;
            			}
            		}
            		
            		//int storyUsage = storyByLanguage.getContent().
            		double popularityIndex = storyCount/maxStoryUsage;
            		rank = Math.round(rank * (1 + popularityIndex * 100));
            		
            		video.setLdRank((int)rank);
            		_publicVideoRepository.save(video);
            	}
            	pageNum++;            	
            }
    	}
    }
      
    @Override
    public List<PublicVideo> RecommendToUser(String userId, String language, String filteredVideos, int records) {
    	
    	List<PublicVideo> recommendVideos = null;
    	try {
    		Profile userProfile = _profileRepository.findByUserId(userId);
    		if(userProfile != null) {    			
    			GeneratedCluster userCluster = _generatedClusterRepository.findByMembers(userId);
    			if(userCluster != null) {
    				// Personalized recommendation
                	List<StoryDetails> storyDetails = _storyDetailsRepository.getDetails();
        			List<String> userPreferences = new ArrayList<String>();
                	userPreferences.add(userProfile.getGender());
                	userProfile.setFilteredVideos(filteredVideos);
                	
                	//Page<Video> createdVideos = _videoRepository.findByUserUuid(userId, new PageRequest(0, 1, new Sort(Direction.DESC, "timestamp")));
                	//long activeTimestamp = createdVideos.getContent().get(0).getTimestamp();
                	Date currentTime = new Date();
                	long thresholdActiveTime = currentTime.getTime() - userProfile.getActiveTime();
                	int thresholdDays = Math.round(thresholdActiveTime/(1000 * 60 * 60 * 24));
        			if(thresholdDays > 15 && userProfile.getActiveTime() != 0) {
        				// user-based recommendation
        				recommendVideos = UserBasedRecommender(userCluster, storyDetails, userProfile);
        				// fallback, if user-based recommendation is not enough
                		// update recommendation list with videos from generic lists
                		int numRecommendVideos = recommendVideos.size();
                    	if(numRecommendVideos < records) {    
                    		for(PublicVideo rv : recommendVideos) {
                    			filteredVideos += ","+rv.getId();
                    		}
                    		// get videos from generic recommendation
                    		List<PublicVideo> fallBackRecommendation = GenericRecommendation(language, filteredVideos, records);                    		
                    		recommendVideos.addAll(fallBackRecommendation);
                    	}
        			}
        			else {
        				// item-based recommendation        			
                    	for(Profile.FavoriteStory favStory : userProfile.getFavStories()) {
                    		userPreferences.add("story_"+favStory.getStoryId());
                    	}
                    	for(Profile.FavoriteCharacter favCharacter : userProfile.getFavCharacters()) {
                    		userPreferences.add("character_"+favCharacter.getCharacterId());
                    	}
                    	
                    	Type listType = new TypeToken<List<PublicVideo>>() {}.getType();
                    	List<PublicVideo> matchedVideos = new Gson().fromJson(Decompress(userCluster.getMatchedVideos()), listType);
                    	
                    	recommendVideos = MatchVideos(matchedVideos, storyDetails, userPreferences, userProfile);
                    	// fallback, if item-based recommendation is not enough
                    	// update recommendation list with user-based filtering
                    	int numRecommendVideos = recommendVideos.size();
                    	if(numRecommendVideos < records) {
                    		for(PublicVideo rv : recommendVideos) {
                    			filteredVideos += ","+rv.getId();
                    		}
                    		userProfile.setFilteredVideos(filteredVideos);
                    		// get videos from user-based recommendation
                    		List<PublicVideo> fallBackRecommendation = UserBasedRecommender(userCluster, storyDetails, userProfile);                    		
                    		recommendVideos.addAll(fallBackRecommendation);
                    		// fallback, if combination of both item-based recommendation and user-based recommendation is not enough
                    		// update recommendation list with videos from generic lists
                    		numRecommendVideos = recommendVideos.size();
                        	if(numRecommendVideos < records) {    
                        		for(PublicVideo rv : recommendVideos) {
                        			if(!filteredVideos.contains(rv.getId())){
                        				filteredVideos += ","+rv.getId();
                        			}
                        		}
                        		// get videos from generic recommendation
                        		fallBackRecommendation = GenericRecommendation(language, filteredVideos, records);                    		
                        		recommendVideos.addAll(fallBackRecommendation);
                        	}
                    	}
        			}
    			}
    			else {
        			// Generic recommendation
        			recommendVideos = GenericRecommendation(language, filteredVideos, records);    			
        		}
    		}
    		else {
    			// Generic recommendation
    			recommendVideos = GenericRecommendation(language, filteredVideos, records);    			
    		}
        	
    		if(recommendVideos.size() > records) {
    			return recommendVideos.subList(0, records);
    		}
    		else {
    			return recommendVideos;
    		}
        	
    	}
    	catch(Exception ex) {
    		System.out.println(ex);
    	}
    	
    	return null;
    }
    
    @Override
    public void GetSimilarUserPreferences(String userId) {
    	
    	Profile userProfile = _profileRepository.findByUserId(userId);
    	
    	int similarityCriteria = 0;
    	String favStories = "";
        for(Profile.FavoriteStory favStory : userProfile.getFavStories()){
        	favStories += favStory.getStoryId()+",";
        	similarityCriteria ++;
        }
        String favCharacters = "";
        for(Profile.FavoriteCharacter favCharacter : userProfile.getFavCharacters()){
        	favCharacters += favCharacter.getCharacterId()+",";
        	similarityCriteria ++;
        }
        
        Set<String> similarPreferences = new HashSet<String>();
        int similarity = 0;
                
        Page<Profile> userProfiles = _profileRepository.findAll(new PageRequest(0, 1500));
        int pageNumbers = userProfiles.getTotalPages();
        int pageNum = 0;
        while(pageNumbers >= pageNum) {
        	if(pageNum > 0) {
        		userProfiles = _profileRepository.findAll(new PageRequest(pageNum, 1500));
        	}
        	
        	for(Profile similarUserProfile : userProfiles) {
        		if(similarUserProfile.equals(userProfile)) {
        			continue;
        		}
        		Set<String> similarUserPreferences = new HashSet<String>();
        		for(Profile.FavoriteStory similarUserFavStory : similarUserProfile.getFavStories()) {
        			// regex for filtering videos
            		Pattern r = Pattern.compile(Long.toString(similarUserFavStory.getStoryId()));        		
        	    	Matcher m = r.matcher(favStories);
        	    	if(m.find()) {
        	    		similarity ++;
        	    	}
        	    	else {
        	    		similarUserPreferences.add("story_"+similarUserFavStory.getStoryId());
        	    	}
        		}
        		
        		for(Profile.FavoriteCharacter similarUserFavCharacter : similarUserProfile.getFavCharacters()) {
        			// regex for filtering videos
            		Pattern r = Pattern.compile(Long.toString(similarUserFavCharacter.getCharacterId()));        		
        	    	Matcher m = r.matcher(favCharacters);
        	    	if(m.find()) {
        	    		similarity ++;
        	    	}
        	    	else {
        	    		similarUserPreferences.add("character_"+similarUserFavCharacter.getCharacterId());
        	    	}
        		}
        		if(similarity/similarityCriteria > 0.7){
        			similarPreferences.addAll(similarUserPreferences);
        		}    		
        	}
        	pageNum++;
        }
        List<String> setToList = new ArrayList<String>();
        setToList.addAll(similarPreferences);
        userProfile.setSimilarPreferences(setToList);
        _profileRepository.save(userProfile);
    	//return similarPreferences;
    }

    private List<PublicVideo> UserBasedRecommender(GeneratedCluster userCluster, List<StoryDetails> storyDetails, Profile userProfile) {
    	List<String> similarUserPreferences = userProfile.getSimilarPreferences();//GetSimilarUserPreferences(userProfile);
    	// user-based recommendation
				
    	Type listType = new TypeToken<List<PublicVideo>>() {}.getType();
    	List<PublicVideo> matchedVideos = new Gson().fromJson(Decompress(userCluster.getMatchedVideos()), listType);
    	
		return MatchVideos(matchedVideos, storyDetails, similarUserPreferences, userProfile);		
    }
    
    private List<PublicVideo> MatchVideos(List<PublicVideo> videos, List<StoryDetails> storyDetails, List<String> clusterBehavior, Profile userProfile) {
        
    	List<PublicVideo> matchedVideos = new ArrayList<PublicVideo>();
    	String userId = "";
    	String language = "";
    	String filteredVideos = "";
    	if(userProfile != null) {
	        userId = userProfile.getUserId();
	        language = userProfile.getLanguage();
	        filteredVideos = userProfile.getFilteredVideos();
    	}
        
        for(PublicVideo video: videos) {        	
        	if(!filteredVideos.isEmpty()) {
        		// regex for filtering videos
        		Pattern r = Pattern.compile(video.getId());        		
    	    	Matcher m = r.matcher(filteredVideos);
    	    	if(m.find()) {
    	    		continue;
    	    	}
        	}
            
            int score = 0;                
            if(userId.equals(video.getUuid()) || (!language.isEmpty() && !language.equalsIgnoreCase(video.getLanguage()))) { 
                continue;
            }
            PublicVideo.Meta videoMeta = video.getMeta();

            if(videoMeta != null) {

                final long storyId = videoMeta.getStory();

                Collection userVideoStoryDetailsMatch = CollectionUtils.select(storyDetails, new Predicate() {
                    public boolean evaluate(Object userStoryDetails) {
                        return ((StoryDetails) userStoryDetails).getStoryId() == storyId;
                    }
                });

                if(userVideoStoryDetailsMatch.size() > 0) {
                    StoryDetails userVideoStoryDetails = (StoryDetails)(userVideoStoryDetailsMatch.toArray())[0];
                    final long characterId = userVideoStoryDetails.getCharacterId();
                    
                    if(clusterBehavior.contains("story_" + storyId)) {
                    	int numStoryUsed = 1;
                    	if(userProfile != null) {
	                    	Collection userFavStory = CollectionUtils.select(userProfile.getFavStories(), new Predicate() {
	                            public boolean evaluate(Object favStory) {
	                                return ((Profile.FavoriteStory) favStory).getStoryId() == storyId;
	                            }
	                        });	     
	                    	if(userFavStory.size() > 0) {
	                    		numStoryUsed = ((Profile.FavoriteStory)userFavStory.toArray()[0]).getCount();
	                    	}
                    	}
                        score += numStoryUsed;
                    }
                    if(clusterBehavior.contains("character_" + characterId)) {
                    	int numCharacterUsed = 1;
                    	if(userProfile != null) {
	                    	Collection userFavCharacter = CollectionUtils.select(userProfile.getFavCharacters(), new Predicate() {
	                            public boolean evaluate(Object favCharacter) {
	                                return ((Profile.FavoriteCharacter) favCharacter).getCharacterId() == characterId;
	                            }
	                        });
	                    	if(userFavCharacter.size() > 0){
	                    		numCharacterUsed = ((Profile.FavoriteCharacter)userFavCharacter.toArray()[0]).getCount();
	                    	}	                    	
                    	}
                        score += numCharacterUsed;
                    }
                    if(clusterBehavior.contains(userVideoStoryDetails.getGender().toLowerCase())) {
                        score++;
                    }
                    // if score is greater than 0,
                    // put video in respective cluster
                    if(score > 0) {
                        video.setScore(score * video.getRank());                                                       
                        matchedVideos.add(video);                                
                    }
                }
            }
        }
        // sort videos by score and rank of the video
        Collections.sort(matchedVideos, new Comparator(){
            @Override
            public int compare(Object v1, Object v2) {
                
                // sort by score
                long score_v1 = ((PublicVideo)v1).getScore();
                long score_v2 = ((PublicVideo)v2).getScore();

                if(score_v1 > score_v2) {
                    return -1;
                }
                else if(score_v1 < score_v2) {
                    return 1;
                }
                else {
                    // sort by rank
                    long rank_v1 = ((PublicVideo)v1).getRank();
                    long rank_v2 = ((PublicVideo)v2).getRank();

                    if(rank_v1 > rank_v2) {
                        return -1;
                    }
                    else if(rank_v1 < rank_v2) {
                        return 1;
                    }
                }
                return 0; 
            }
        });
        
        return matchedVideos;
    }
    
    private List<PublicVideo> GenericRecommendation(String language, String filterVideos, int records) {
    	
    	String[] filterVideoIds = filterVideos.split(",");    	
    	//List<PublicVideo> videos = _publicVideoRepository.findByLanguageAndIdNotIn(language, filterVideoIds);
    	
    	Page<PublicVideo> videos = _publicVideoRepository.findByLanguageAndIdNotIn(language, filterVideoIds, new PageRequest(0, 15, new Sort(Direction.DESC, "ldRank")));
    	List<PublicVideo> videoList = videos.getContent();
    	
    	if(videos.getNumberOfElements() < records) {
    		List<String> filterVideoIdsList = Arrays.asList(filterVideoIds);
    		for(PublicVideo pv : videoList) {  
    			filterVideoIdsList.add(pv.getId());
    		}
    		String[] filterVideoIdsArray = new String[filterVideoIdsList.size()];    		
    		int recordsToFetch = records - videos.getNumberOfElements();
    		videos = _publicVideoRepository.findByIdNotIn(filterVideoIdsList.toArray(filterVideoIdsArray), new PageRequest(0, recordsToFetch, new Sort(Direction.DESC, "rank")));
    		if(!videoList.isEmpty()) {
    			videoList.addAll(videos.getContent());
    		}
    		videoList = videos.getContent();
    		
    	}
    	return videoList;
    }

    public static byte[] Compress(String str) {
        
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip;
        byte[] outStr = null;
		try {
			gzip = new GZIPOutputStream(out);
			gzip.write(str.getBytes());
	        gzip.close();
	        outStr = out.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return outStr;
     }
    
    public static String Decompress(byte[] bytes) {        
        
    	String outStr = "";
        GZIPInputStream gis;
		try {
			gis = new GZIPInputStream(new ByteArrayInputStream(bytes));
			BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));	        
	        String line;
	        while ((line=bf.readLine())!=null) {
	          outStr += line;
	        }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
                
        return outStr;
     }
}

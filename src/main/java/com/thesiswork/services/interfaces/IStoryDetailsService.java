/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.services.interfaces;

import java.util.List;

import com.thesiswork.model.StoryDetails;

/**
 *
 * @author keshant
 */
public interface IStoryDetailsService {

    public List<String> getFeatures();
    
    public List<StoryDetails> getStoryDetails();
}

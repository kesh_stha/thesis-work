package com.thesiswork.services.interfaces;

import com.thesiswork.model.Profile;

public interface IProfileService {
	
	public Profile getUserDetails(String userId);
}

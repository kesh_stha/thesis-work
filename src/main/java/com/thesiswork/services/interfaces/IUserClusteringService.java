/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.services.interfaces;

import java.util.List;
import java.util.Map;

/**
 *
 * @author keshant
 */
public interface IUserClusteringService {
    
    public void GenerateClusters(List<String> features, Map<String, String> filePath);
    
    public List<Map<String, List<String>>> GetClusterBehavior(List<String> features, Map<String, String> filePath);
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesiswork.services.interfaces;

import com.thesiswork.model.PublicVideo;
import com.thesiswork.model.StoryDetails;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;

/**
 *
 * @author keshant
 */
public interface IPublicVideoService {
    
Page<PublicVideo> getVideos(int page, int size);
    
    List<PublicVideo> getVideosByMetaBundleString(String bundleString);
    
    List<PublicVideo> getVideosByTitleRegex(String title);
    
    void AssignToCluster(List<Map<String, List<String>>> clusters,  List<StoryDetails> storyDetails, String clusteredVideosPath);
    
    List<PublicVideo> RecommendToUser(String userId, String language, String filteredVideos, int records);
    
    public void UpdateLanguageSpecificRank();
    
    public void GetSimilarUserPreferences(String userId);
}
